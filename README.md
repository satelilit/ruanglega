Mysql + WP on GKE

Prerequisites :

Create GKE cluster first !

How to run : 

- Create PV first
`kubectl apply -f mysql/mysql-pv.yaml`
`kubectl apply -f wp/wp-pv.yaml`

- Create statefulsets
`kubectl create -f mysql/mysql.yaml`
`kubectl create -f wp/wp.yaml`

- Create service for exposing the apps
`kubectl create -f mysql/mysql-service.yaml`
`kubectl create -f wp/wp-service.yaml`


Cleaning up :
`bash cleanup.sh`

